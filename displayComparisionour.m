
clc;
clear;
addpath('./Evaluation/');

%%1--Video Name:
%% Benchmark dataset
%MIL和TLD都有问题

% fileName = 'faceocc1';
% fileName = 'faceocc2';  
% fileName = 'boy'; 
%  fileName = 'dog1';      
%  fileName = 'crossing'; 
%  fileName = 'david2';
%   fileName = 'doll';   
% fileName = 'dudek';
 %  fileName = 'girl';
 %  fileName = 'shaking'; 
%  fileName =  'freeman1';  
%   fileName = 'fish';
%  fileName = 'mountainBike'; 
%  fileName = 'singer2';    
%  fileName = 'subway';     
%   fileName = 'car4';  %重新跑一遍
%  fileName = 'singer1';   
% fileName = 'carDark'; %重新跑一遍
% fileName = 'walking';
% fileName = 'freeman3';
% fileName = 'walking2';  %缺MIL
%  fileName = 'woman';
% fileName = 'sylvester';  %重新跑一遍
%  fileName = 'mhyang';
%  fileName = 'football';
  fileName = 'bird';
 % fileName = 'board';

%%  Additional data sets:
for k= 1:99
frameIndex(k) = k;
end
%%2--Load data
filePath = [ '.\Results\' fileName '\' ];
%%(14)GT
load([filePath fileName '_gt_rs.mat']);
load([filePath fileName '_DLT_rs.mat']);

[ overlapRateDLT]   = overlapEvaluationQuad(DLTCornersAll, gtCornersAll, frameIndex);
ORE.DLT   = overlapRateDLT;
ORE_M.DLT   = mean(overlapRateDLT);
ORE_S.DLT   = sum(overlapRateDLT>0.5);
save([fileName '_overlapRateEvaluation.mat'], 'ORE', 'ORE_M', 'ORE_S', 'frameIndex');

[ centerErrorDLT ]   = centerErrorEvaluation(DLTCenterAll, gtCenterAll, frameIndex);
CEE.DLT   = centerErrorDLT;
CEE_M.DLT   = mean(centerErrorDLT);

save([fileName '_centerErrorEvaluation.mat'], 'CEE', 'CEE_M', 'frameIndex');


