load caviar2_gt_rs.mat
len = 500;
box = zeros(len,4);
for i = 1:len
    temp = gtCornersAll{1,i};
    box(i,1) = temp(1,1);
    box(i,2) = temp(2,1);
    box(i,3) = temp(1,3)-temp(1,1);
    box(i,4) = temp(2,3)-temp(2,1);
end
save box;