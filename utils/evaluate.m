clc;
clear all;
load  boy_ivt;
title = 'boy';
result = results{1,1}.res;
TotalFrameNum = results{1,1}.len;
for num = 1:TotalFrameNum
    if  num <= size(result,1)
        est = result(num,:);
        [ center corners ] = p_to_box([32 32], est);
    end
    IVTCenterAll{num}  = center;      
    IVTCornersAll{num} = corners;
end
save([ title '_IVT_rs.mat'], 'IVTCenterAll', 'IVTCornersAll');