
clc;
clear;
addpath('./Evaluation/');

%%1--Video Name:
%%  Data sets reported in the TIP paper 
%L1APG和MTT都有问题

% fileName = 'faceocc1';
% fileName = 'faceocc2';  
%  fileName = 'boy';
% fileName = 'Caviar2';
%  fileName = 'fleetface';
%  fileName = 'dog1';
%  fileName = 'crossing'; %
%  fileName = 'david2';
%   fileName = 'doll';
%   fileName = 'shaking';
%  fileName =  'freeman1';
%  fileName = 'fish';
  fileName = 'mountainBike'; %our_rs结果重跑一遍
%  fileName = 'singer2';
%  fileName = 'subway';
% fileName = 'car4';  %gt有问题
% fileName = 'Singer1';
% fileName = 'DavidIndoor';
% fileName = 'carDark';
% fileName = 'walking';
% fileName = 'walking2';
% fileName = 'Jumping';
%  fileName = 'woman';
% fileName = 'Cliffbar';
%%  Additional data sets:
% fileName = 'DavidOutdoor';
% fileName = 'Stone';
% fileName = 'Girl';

for k= 1:228
frameIndex(k) = k;
end
%%2--Load data
filePath = [ '.\results\' fileName '\' ];
%%(1)IVT Tracker
load([filePath fileName '_IVT_rs.mat']); 
%%(2)L1APG Tracker
load([filePath fileName '_L1APG_rs.mat']);
%%(3)MTT Tracker
load([filePath fileName '_MTT_rs.mat']);
%%(4)SCM Tracker
%load([filePath fileName '_SCM_rs.mat']);
%%(5)MIL Tracker
%load([filePath fileName '_mil_rs.mat']);
%%(6)ASLA Tracker
load([filePath fileName '_ASLA_rs.mat']);
%%(7)My Tracker
load([filePath fileName '_our_rs.mat']);
%%(8)GT
load([filePath fileName '_gt_rs.mat']);

%%3--Overlap Rate Evaluation
%%(1)ivt Tracker
[ overlapRateIVT ]   = overlapEvaluationQuad(IVTCornersAll, gtCornersAll, frameIndex);
%%(2)L1 Tracker
[ overlapRateL1APG ]    = overlapEvaluationQuad(L1APGCornersAll, gtCornersAll, frameIndex);
%%(3)PN Tracker
%[ overlapRateSCM ]    = overlapEvaluationQuad(SCMCornersAll, gtCornersAll, frameIndex);
%%(4)VTD Tracker
[ overlapRateMTT ]   = overlapEvaluationQuad(MTTCornersAll, gtCornersAll, frameIndex);
%%(5)ASLA Tracker
[ overlapRateASLA ]   = overlapEvaluationQuad(ASLACornersAll, gtCornersAll, frameIndex);
%%(6)Frag Tracker
%[ overlapRateFrag ]  = overlapEvaluationQuad(fragCornersAll, gtCornersAll, frameIndex);
%%(7)My Tracker
[ overlapRateOUR ] = overlapEvaluationQuad(ourCornersAll, gtCornersAll, frameIndex);
ORE = [];   %%Overlap Rate Evaluation
ORE.IVT   = overlapRateIVT;
ORE.L1APG    = overlapRateL1APG;
%ORE.SCM    = overlapRateSCM;
ORE.MTT   = overlapRateMTT;
ORE.ASLA   = overlapRateASLA;
ORE.OUR  = overlapRateOUR;
%ORE.SRPCA = overlapRateSRPCA;
ORE_M = []; %%Overlap Rate Evaluation (Mean)
ORE_M.IVT   = mean(overlapRateIVT);
ORE_M.L1APG    = mean(overlapRateL1APG);
%ORE_M.SCM    = mean(overlapRateSCM);
ORE_M.MTT   = mean(overlapRateMTT);
ORE_M.ASLA   = mean(overlapRateASLA);
ORE_M.OUR  = mean(overlapRateOUR);
%ORE_M.SRPCA = mean(overlapRateSRPCA)
ORE_S = []; %%Overlap Rate Evaluation (Successful Tracking Frames)
ORE_S.IVT   = sum(overlapRateIVT>0.5);
ORE_S.L1APG    = sum(overlapRateL1APG>0.5);
%ORE_S.SCM    = sum(overlapRateSCM>0.5);
ORE_S.MTT   = sum(overlapRateMTT>0.5);
ORE_S.ASLA   = sum(overlapRateASLA>0.5);
ORE_S.OUR  = sum(overlapRateOUR>0.5);
%ORE_S.SRPCA = sum(overlapRateSRPCA>0.5);
save([fileName '_overlapRateEvaluation.mat'], 'ORE', 'ORE_M', 'ORE_S', 'frameIndex');

%%4--Center Error Evaluation
%%(1)IPCA(IVT) Tracker
[ centerErrorIVT ]   = centerErrorEvaluation(IVTCenterAll, gtCenterAll, frameIndex);
%%(2)L1 Tracker
[ centerErrorL1APG ]    = centerErrorEvaluation(L1APGCenterAll, gtCenterAll, frameIndex);
%%(3)PN Tracker
%[ centerErrorSCM ]    = centerErrorEvaluation(SCMCenterAll, gtCenterAll, frameIndex);
%%(4)VTD Tracker
[ centerErrorMTT ]   = centerErrorEvaluation(MTTCenterAll, gtCenterAll, frameIndex);
%%(5)MIL Tracker
[ centerErrorASLA ]   = centerErrorEvaluation(ASLACenterAll, gtCenterAll, frameIndex);
%%(6)Frag Tracker
[ centerErrorOUR ]  = centerErrorEvaluation(ourCenterAll, gtCenterAll, frameIndex);
%%(7)My Tracker
%[ centerErrorSRPCA ] = centerErrorEvaluation(srpcaCenterAll, gtCenterAll, frameIndex);
CEE = [];   %%Center Error Evaluation
CEE.IVT   = centerErrorIVT;
CEE.L1APG    = centerErrorL1APG;
%CEE.SCM    = centerErrorSCM;
CEE.MTT   = centerErrorMTT;
CEE.ASLA   = centerErrorASLA;
CEE.OUR  = centerErrorOUR;
%CEE.SRPCA = centerErrorSRPCA;
CEE_M = []; %%Center Error Evaluation (Mean)
CEE_M.IVT   = mean(centerErrorIVT);
CEE_M.L1APG = mean(centerErrorL1APG);
%CEE_M.PN    = mean(centerErrorPN(~isnan(centerErrorPN)));
%CEE_M.SCM   = mean(centerErrorSCM); 
CEE_M.MTT   = mean(centerErrorMTT);
CEE_M.ASLA   = mean(centerErrorASLA);
CEE_M.OUR  = mean(centerErrorOUR);
%CEE_M.SRPCA = mean(centerErrorSRPCA)
save([fileName '_centerErrorEvaluation.mat'], 'CEE', 'CEE_M', 'frameIndex');

%%5--Display Overlap Rate Evaluation
%figure(1);
%hold on;
%plot(frameIndex, ORE.PCA,   'B--', 'linewidth', 2.5); 
%hold on; 
%plot(frameIndex, ORE.L1 ,   'G--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.PN,    'K--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.VTD,   'Y--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.MIL,   'M--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.Frag,  'C--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.SRPCA, 'R',   'linewidth', 2.5); 
%xlabel('Frame Number','fontsize',18, 'fontweight','bold');
%ylabel('Overlap Rate','fontsize',18, 'fontweight','bold');
%hold off;
%set(gca, 'fontsize', 18, 'fontweight', 'bold', 'box', 'on');

%%6--Display Center Error Evaluation
%figure(2);
%hold on;
%plot(frameIndex, CEE.PCA,   'B--', 'linewidth', 2.5); 
%hold on; 
%plot(frameIndex, CEE.L1,    'G--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.PN,    'K--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.VTD,   'Y--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.MIL,   'M--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.Frag,  'C--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.SRPCA, 'R',   'linewidth', 2.5); 
%xlabel('Frame Number','fontsize',18, 'fontweight','bold');
%ylabel('Center Error','fontsize',18, 'fontweight','bold');
%hold off;
%set(gca, 'fontsize', 18, 'fontweight', 'bold', 'box', 'on');