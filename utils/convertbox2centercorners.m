
clc;
clear all;
load  boy_Struck;
title = 'boy';
algorithm = 'Struck';
result = results{1,1}.res;
frameNum = results{1,1}.len;

gtCornersAll = cell(1,frameNum);
  gtCenterAll = cell(1,frameNum);
  for i =1:frameNum
      temp1 = result(i,1)+ result(i,3)/2;
      temp1 = [temp1; result(i,2)+ result(i,4)/2];
      gtCenterAll{1,i} = temp1;    
     
      temp2 = [result(i,1); result(i,2)];
      cornertemp = temp2;
      temp2 = [result(i,1)+result(i,3); result(i,2)];
      cornertemp = [cornertemp, temp2];
      temp2 = [result(i,1)+result(i,3); result(i,2)+ result(i,4)];
      cornertemp = [cornertemp, temp2];
      temp2 = [result(i,1); result(i,2)+ result(i,4)];
      cornertemp = [cornertemp, temp2];
      temp2 = [result(i,1); result(i,2)];
      cornertemp = [cornertemp, temp2];
      gtCornersAll{1,i} = cornertemp; 
   
      
      temp1 =[];
      temp2 = [];
      cornertemp = [];   
  end
  
    
fileName =sprintf('results/%s/%s_%s_rs.mat',title, title, algorithm);
save( fileName, 'StruckCenterAll', 'StruckCornersAll');