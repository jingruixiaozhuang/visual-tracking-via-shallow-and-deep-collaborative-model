
clc;
clear;
addpath('./Evaluation/');

%%1--Video Name:
%% Benchmark dataset
%MIL和TLD都有问题

% fileName = 'faceocc1';
% fileName = 'faceocc2';  
%  fileName = 'boy';
%  fileName = 'fleetface';  %缺TLD
%  fileName = 'dog1';      %
%  fileName = 'crossing'; %
%  fileName = 'david2';
%   fileName = 'doll';   % 缺gt
%   fileName = 'shaking';  
%  fileName =  'freeman1';  %缺TLD
%   fileName = 'fish';
%  fileName = 'mountainBike'; %CSK_rs结果重跑一遍  缺MIL
%  fileName = 'singer2';     % 缺gt
%  fileName = 'subway';     % 
% fileName = 'car4';  %gt有问题
%  fileName = 'Singer1';   
% fileName = 'carDark';
% fileName = 'walking';
% fileName = 'walking2'; %缺MIL
%  fileName = 'woman';
% fileName = 'sylvester';  缺gt
%%  Additional data sets:

for k= 1:1345
frameIndex(k) = k;
end
%%2--Load data
filePath = [ '.\results\' fileName '\' ];
%%(1)IVT Tracker
load([filePath fileName '_IVT_rs.mat']);
%%(2)L1APG Tracker
load([filePath fileName '_L1APG_rs.mat']);
%%(3)MTT Tracker
load([filePath fileName '_MTT_rs.mat']);
%%(4)ASLA Tracker
load([filePath fileName '_ASLA_rs.mat']);
%%(5)OSPT Tracker
load([filePath fileName '_OSPT_rs.mat']);
%%(6)CT Tracker
load([filePath fileName '_CT_rs.mat']); 
%%(7)MIL Tracker
load([filePath fileName '_MIL_rs.mat']);
%%(8)Struck Tracker
load([filePath fileName '_Frag_rs.mat']);
%%(9)TLD Tracker
load([filePath fileName '_TLD_rs.mat']);
%%(10)Struck Tracker
load([filePath fileName '_Struck_rs.mat']);
%%(11)VTD Tracker
load([filePath fileName '_VTD_rs.mat']);
%%(12)CSK Tracker
load([filePath fileName '_CSK_rs.mat']);
%%(13)DLT
load([filePath fileName '_DLT_rs.mat']);
%%(14)GT
load([filePath fileName '_gt_rs.mat']);
%%(15)Our Tracker
load([filePath fileName '_our_rs.mat']);


%%3--Overlap Rate Evaluation
%%(1)IVT Tracker
[ overlapRateIVT ] = overlapEvaluationQuad(IVTCornersAll, gtCornersAll, frameIndex);
%%(2)L1APG Tracker
[ overlapRateL1APG ] = overlapEvaluationQuad(L1APGCornersAll, gtCornersAll, frameIndex);
%%(3)MTT Tracker
[ overlapRateMTT ] = overlapEvaluationQuad(MTTCornersAll, gtCornersAll, frameIndex);
%%(4)ASLA Tracker
[ overlapRateASLA ] = overlapEvaluationQuad(ASLACornersAll, gtCornersAll, frameIndex);
%%(5)OSPT Tracker
[ overlapRateOSPT]   = overlapEvaluationQuad(OSPTCornersAll, gtCornersAll, frameIndex);
%%(6)CT Tracker
[ overlapRateCT ] = overlapEvaluationQuad(CSKCornersAll, gtCornersAll, frameIndex);
%%(7)MIL Tracker
[ overlapRateMIL]    = overlapEvaluationQuad(MILCornersAll, gtCornersAll, frameIndex);
%%(8)PN Tracker
[ overlapRateFRAG ]    = overlapEvaluationQuad(FRAGCornersAll, gtCornersAll, frameIndex);
%%(9)TLD Tracker
[ overlapRateTLD ]   = overlapEvaluationQuad(TLDCornersAll, gtCornersAll, frameIndex);
%%(10)Struck Tracker
[ overlapRateStruck ]  = overlapEvaluationQuad(StruckCornersAll, gtCornersAll, frameIndex);
%%(11)VTD Tracker
[ overlapRateVTD ]   = overlapEvaluationQuad(VTDCornersAll, gtCornersAll, frameIndex);
%%(12)CSK Tracker
[ overlapRateCSK ] = overlapEvaluationQuad(CSKCornersAll, gtCornersAll, frameIndex);
%%(13)DLT Tracker
[ overlapRateDLT ] = overlapEvaluationQuad(DLTCornersAll, gtCornersAll, frameIndex);
%%(7)My Tracker
[ overlapRateOur ] = overlapEvaluationQuad(ourCornersAll, gtCornersAll, frameIndex);



ORE = [];   %%Overlap Rate Evaluation
ORE.IVT   = overlapRateIVT;
ORE.L1APG = overlapRateL1APG;
ORE.MTT   = overlapRateMTT;
ORE.ASLA   = overlapRateASLA;
ORE.OSPT   = overlapRateOSPT;
ORE.CT   = overlapRateCT;
ORE.MIL    = overlapRateMIL;
ORE.FRAG = overlapRateFRAG;
ORE.TLD   = overlapRateTLD;
ORE.Struck   = overlapRateStruck;
ORE.VTD   = overlapRateVTD;
ORE.CSK  = overlapRateCSK;
ORE.DLT = overlapRateDLT;
ORE.Our   = overlapRateOur;


ORE_M = []; %%Overlap Rate Evaluation (Mean)
ORE_M.IVT  = mean(overlapRateIVT);
ORE_M.L1APG   = mean(overlapRateL1APG);
ORE_M.MTT   = mean(overlapRateMTT);
ORE_M.ASLA   = mean(overlapRateASLA);
ORE_M.OSPT   = mean(overlapRateOSPT);
ORE_M.CT   = mean(overlapRateCT);
ORE_M.MIL    = mean(overlapRateMIL);
ORE_M.FRAG = mean(overlapRateFRAG);
ORE_M.TLD   = mean(overlapRateTLD);
ORE_M.Struck   = mean(overlapRateStruck);
ORE_M.VTD   = mean(overlapRateVTD);
ORE_M.CSK  = mean(overlapRateCSK);
ORE_M.DLT   = mean(overlapRateDLT);
ORE_M.Our = mean(overlapRateOur);

ORE_S = []; %%Overlap Rate Evaluation (Successful Tracking Frames)
ORE_S.IVT   = sum(overlapRateIVT>0.5);
ORE_S.L1APG   = sum(overlapRateL1APG>0.5);
ORE_S.MTT   = sum(overlapRateMTT>0.5);
ORE_S.ASLA   = sum(overlapRateASLA>0.5);
ORE_S.OSPT   = sum(overlapRateOSPT>0.5);
ORE_S.CT   = sum(overlapRateCT>0.5);
ORE_S.MIL    = sum(overlapRateMIL>0.5);
ORE_S.FRAG = sum(overlapRateFRAG>0.5);
ORE_S.TLD   = sum(overlapRateTLD>0.5);
ORE_S.Struck    = sum(overlapRateStruck>0.5);
ORE_S.VTD   = sum(overlapRateVTD>0.5);
ORE_S.CSK  = sum(overlapRateCSK>0.5);
ORE_S.DLT = sum(overlapRateFRAG>0.5);
ORE_S.Our   = sum(overlapRateOur>0.5);
save([fileName '_overlapRateEvaluation.mat'], 'ORE', 'ORE_M', 'ORE_S', 'frameIndex');

%%4--Center Error Evaluation
%%(1)IVT Tracker
[ centerErrorIVT ]   = centerErrorEvaluation(IVTCenterAll, gtCenterAll, frameIndex);
%%(2)L1APG Tracker
[ centerErrorL1APG ]    = centerErrorEvaluation(L1APGCenterAll, gtCenterAll, frameIndex);
%%(3)MTT Tracker
[ centerErrorMTT]    = centerErrorEvaluation(MTTCenterAll, gtCenterAll, frameIndex);
%%(4)ASLA Tracker
[ centerErrorASLA ]   = centerErrorEvaluation(ASLACenterAll, gtCenterAll, frameIndex);
%%(5)OSPT Tracker
[ centerErrorOSPT ]   = centerErrorEvaluation(OSPTCenterAll, gtCenterAll, frameIndex);
%%(6)CT Tracker
[ centerErrorCT ]  = centerErrorEvaluation(CTCenterAll, gtCenterAll, frameIndex);
%%(7)MIL Tracker
[ centerErrorMIL ]  = centerErrorEvaluation(MILCenterAll, gtCenterAll, frameIndex);
%%(8)FRAG Tracker
[ centerErrorFRAG ]  = centerErrorEvaluation(FRAGCenterAll, gtCenterAll, frameIndex);
%%(9)TLD Tracker
[ centerErrorTLD ]  = centerErrorEvaluation(TLDCenterAll, gtCenterAll, frameIndex);
%%(10)Struck Tracker
[ centerErrorStruck ]  = centerErrorEvaluation(StruckCenterAll, gtCenterAll, frameIndex);
%%(11)VTD Tracker
[ centerErrorVTD ]  = centerErrorEvaluation(VTDCenterAll, gtCenterAll, frameIndex);
%%(12)CSK Tracker
[ centerErrorCSK ]  = centerErrorEvaluation(CSKCenterAll, gtCenterAll, frameIndex);
%%(13)DLT Tracker
[ centerErrorDLT ]  = centerErrorEvaluation(DLTCenterAll, gtCenterAll, frameIndex);
%%(14)My Tracker
[ centerErrorOur ] = centerErrorEvaluation(OurCenterAll, gtCenterAll, frameIndex);


CEE = [];   %%Center Error Evaluation
CEE.IVT   = centerErrorIVT;
CEE.L1APG   = centerErrorL1APG;
CEE.MTT   = centerErrorMTT;
CEE.ASLA   = centerErrorASLA;
CEE.OSPT   = centerErrorOSPT;
CEE.CT   = centerErrorCT;
CEE.MIL    = centerErrorMIL;
CEE.FRAG   = centerErrorFRAG;
CEE.TLD   = centerErrorTLD;
CEE.Struck    = centerErrorStruck;
CEE.VTD   = centerErrorVTD;
CEE.CSK  = centerErrorCSK;
CEE.DLT = centerErrorDLT;
CEE.Our   = centerErrorOur;

CEE_M = []; %%Center Error Evaluation (Mean)
CEE_M.IVT   = mean(centerErrorIVT);
CEE_M.L1APG   = mean(centerErrorL1APG);
CEE_M.MTT   = mean(centerErrorMTT);
CEE_M.ASLA   = mean(centerErrorASLA);
CEE_M.OSPT   = mean(centerErrorOSPT);
CEE_M.CT   = mean(centerErrorCT);
CEE_M.MIL = mean(centerErrorMIL);
CEE_M.FRAG   = mean(centerErrorFRAG);
CEE_M.TLD    = mean(centerErrorTLD(~isnan(centerErrorTLD)));
CEE_M.Struck   = mean(centerErrorStruck); 
CEE_M.VTD   = mean(centerErrorVTD);
CEE_M.CSK  = mean(centerErrorCSK);
CEE_M.DLT = mean(centerErrorDLT);
CEE_M.Our   = mean(centerErrorOur);
save([fileName '_centerErrorEvaluation.mat'], 'CEE', 'CEE_M', 'frameIndex');

%%5--Display Overlap Rate Evaluation
%figure(1);
%hold on;
%plot(frameIndex, ORE.PCA,   'B--', 'linewidth', 2.5); 
%hold on; 
%plot(frameIndex, ORE.L1 ,   'G--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.PN,    'K--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.VTD,   'Y--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.MIL,   'M--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.Struck,  'C--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.FRAG, 'R',   'linewidth', 2.5); 
%xlabel('Frame Number','fontsize',18, 'fontweight','bold');
%ylabel('Overlap Rate','fontsize',18, 'fontweight','bold');
%hold off;
%set(gca, 'fontsize', 18, 'fontweight', 'bold', 'box', 'on');

%%6--Display Center Error Evaluation
%figure(2);
%hold on;
%plot(frameIndex, CEE.PCA,   'B--', 'linewidth', 2.5); 
%hold on; 
%plot(frameIndex, CEE.L1,    'G--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.PN,    'K--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.VTD,   'Y--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.MIL,   'M--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.Struck,  'C--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.FRAG, 'R',   'linewidth', 2.5); 
%xlabel('Frame Number','fontsize',18, 'fontweight','bold');
%ylabel('Center Error','fontsize',18, 'fontweight','bold');
%hold off;
%set(gca, 'fontsize', 18, 'fontweight', 'bold', 'box', 'on');