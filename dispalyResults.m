function dispalyResults
%%Algorithms:
algorithm = [{'IVT'},...            %IVT/IPCA Tracker
             {'ASLA'},...             %L1   Tracker
             {'OSPT'},...             %PN   Tracker
             {'CT'},...            %VTD  Tracker
             {'Struck'},...            %MIL  Tracker
             {'DLT'},... 
             {'our'}           %our
            ];
%%Color:
color     = [ {'b'},...       
             {'g'},...      
             {'k'},...         
             {'y'},...           
             {'m'},...           
             {'c'},...           
             {'r'}...       
        
            ];

%%VideoName
% videoName = 'Lemming';
videoName = 'caviar2';
imageSize = [384, 288];
frameNum  = 500;
path = 'E:\paper\benchmark\benchmark data\caviar2\img\';

%%FontSize && LineWidth
% fontSize  = 30;     lineWidth = 3.5;
fontSize  = 18;     lineWidth = 2.7;

%%结果文件或真值文件路径：
filePath  = ['Results' '\' videoName];  

%%视频数据信息：[ 宽 高 帧数 ]
%temp      = importdata(['Data\' videoName '\' 'datainfo.txt' ]);
%imageSize = [ temp(2) temp(1) ];

%%Load Results:
for num = 1:length(algorithm)
    load([ filePath '\' videoName '_' algorithm{num} '_' 'rs.mat' ]);
end

figure('position',[ 100 100 imageSize(1) imageSize(2) ]); 
set(gcf,'DoubleBuffer','on','MenuBar','none');
 nz	= strcat('%0',num2str(1),'d');
for num = 1:frameNum
    %number of zeros in the name of image
       
        id = sprintf(nz,num);
        framePath = strcat(path,id,'.jpg');
    
    
    
    image  = imread(framePath);
    axes('position', [0 0 1.0 1.0]);
    if  size(image,3) == 3
    imagesc(image, [0,1]); 
    else
      imshow( double(image)/256, [0,1]);
    end
    hold on; 

    numStr = sprintf('#%04d', num);
    text(10,20,numStr,'Color', 'r', 'FontWeight', 'bold', 'FontSize', fontSize);

    corner = IVTCornersAll{1,num};
    line(corner(1,:), corner(2,:), 'Color', 'b', 'LineWidth', lineWidth); 
    corner = ASLACornersAll{1,num};
    line(corner(1,:), corner(2,:), 'Color', 'g', 'LineWidth', lineWidth);
    corner = OSPTCornersAll{1,num};
    line(corner(1,:), corner(2,:), 'Color', 'k', 'LineWidth', lineWidth); 
    corner = CTCornersAll{1,num};
    line(corner(1,:), corner(2,:), 'Color', 'y', 'LineWidth', lineWidth); 
    corner = StruckCornersAll{1,num};
    line(corner(1,:), corner(2,:), 'Color', 'm', 'LineWidth', lineWidth);
    corner = DLTCornersAll{1,num};
    line(corner(1,:), corner(2,:), 'Color', 'c', 'LineWidth', lineWidth);    
    corner = ourCornersAll{1,num};
    line(corner(1,:), corner(2,:), 'Color', 'r', 'LineWidth', lineWidth);    
 

    axis off;
    hold off;
    drawnow;
    savePath = sprintf('Dump/%s/%s_%s_%04d.jpg',videoName, videoName, 'rs', num);
    imwrite(frame2im(getframe(gcf)),savePath);
    clf;
end