
clc;
clear;
addpath('./Evaluation/');

%%1--Video Name:
%% Benchmark dataset
%MIL��TLD��������

% fileName = 'faceocc1';
% fileName = 'faceocc2';  
%  fileName = 'boy';
 % fileName = 'board';
 % fileName = 'fleetface';  
 % fileName = 'dog1';      
  fileName = 'crossing'; 
%  fileName = 'david2';
%   fileName = 'doll';   
%  fileName = 'dudek';
%   fileName = 'girl';
%   fileName = 'shaking'; 
%   fileName =  'freeman1';  %ȱTLD
%   fileName = 'fish';
%  fileName = 'mountainbike'; %�������һ��  ȱMIL
%  fileName = 'singer2';    
%  fileName = 'subway';     
%   fileName = 'car4';  %������һ��
%  fileName = 'singer1';   
% fileName = 'carDark'; %������һ��
% fileName = 'walking';
% fileName = 'freeman3';
% fileName = 'walking2';  %ȱMIL
 % fileName = 'woman';
 %  fileName = 'bird';
% fileName = 'sylvester';  %������һ��
%  fileName = 'mhyang';
%  fileName = 'football';
 % fileName = 'caviar2';
%%  Additional data sets:

for k= 1:120
frameIndex(k) = k;
end
%%2--Load data
filePath = './results_discriminative/';
%%(1)IVT Tracker
%load([filePath fileName '_IVT_rs.mat']);
%%(4)ASLA Tracker
%load([filePath fileName '_ASLA_rs.mat']);
%%(5)OSPT Tracker
%load([filePath fileName '_OSPT_rs.mat']);
%%(2)CXT Tracker
%load([filePath fileName '_CXT_rs.mat']);
%%(3)CPF Tracker
%load([filePath fileName '_CPF_rs.mat']);
%%(6)CT Tracker
%load([filePath fileName '_CT_rs.mat']); 
%%(7)MIL Tracker
%load([filePath fileName '_MIL_rs.mat']);
%%(8)Struck Tracker
%load([filePath fileName '_Frag_rs.mat']);
%%(9)TLD Tracker
%load([filePath fileName '_TLD_rs.mat']);
%%(10)Struck Tracker
%load([filePath fileName '_Struck_rs.mat']);
%%(11)DFT Tracker
%load([filePath fileName '_DFT_rs.mat']);
%%(12)CSK Tracker
%load([filePath fileName '_CSK_rs.mat']);
%%(13)DLT
%load([filePath fileName '_DLT_rs.mat']);
%%(14)GT
load([filePath fileName '_gt_rs.mat']);
%%(15)Our Tracker
load([filePath fileName '_our_rs.mat']);


%%3--Overlap Rate Evaluation
%%(7)My Tracker
[ overlapRateOur ] = overlapEvaluationQuad(ourCornersAll, gtCornersAll, frameIndex);



ORE = [];   %%Overlap Rate Evaluation

ORE.Our   = overlapRateOur;


ORE_M = []; %%Overlap Rate Evaluation (Mean)

ORE_M.Our = mean(overlapRateOur);

ORE_S = []; %%Overlap Rate Evaluation (Successful Tracking Frames)
ORE_S.Our   = sum(overlapRateOur>0.5);
save(['./results_discriminative/' fileName '_overlapRateEvaluation.mat'], 'ORE', 'ORE_M', 'ORE_S', 'frameIndex');

%%(14)My Tracker
[ centerErrorOur ] = centerErrorEvaluation(ourCenterAll, gtCenterAll, frameIndex);


CEE = [];   %%Center Error Evaluation
CEE.Our   = centerErrorOur;

CEE_M = []; %%Center Error Evaluation
CEE_M.Our   = mean(centerErrorOur);
save(['./results_discriminative/' fileName '_centerErrorEvaluation.mat'], 'CEE', 'CEE_M', 'frameIndex');

%%5--Display Overlap Rate Evaluation
%figure(1);
%hold on;
%plot(frameIndex, ORE.PCA,   'B--', 'linewidth', 2.5); 
%hold on; 
%plot(frameIndex, ORE.L1 ,   'G--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.PN,    'K--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.DFT,   'Y--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.MIL,   'M--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.Struck,  'C--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, ORE.FRAG, 'R',   'linewidth', 2.5); 
%xlabel('Frame Number','fontsize',18, 'fontweight','bold');
%ylabel('Overlap Rate','fontsize',18, 'fontweight','bold');
%hold off;
%set(gca, 'fontsize', 18, 'fontweight', 'bold', 'box', 'on');

%%6--Display Center Error Evaluation
%figure(2);
%hold on;
%plot(frameIndex, CEE.PCA,   'B--', 'linewidth', 2.5); 
%hold on; 
%plot(frameIndex, CEE.L1,    'G--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.PN,    'K--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.DFT,   'Y--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.MIL,   'M--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.Struck,  'C--', 'linewidth', 2.5); 
%hold on;
%plot(frameIndex, CEE.FRAG, 'R',   'linewidth', 2.5); 
%xlabel('Frame Number','fontsize',18, 'fontweight','bold');
%ylabel('Center Error','fontsize',18, 'fontweight','bold');
%hold off;
%set(gca, 'fontsize', 18, 'fontweight', 'bold', 'box', 'on');