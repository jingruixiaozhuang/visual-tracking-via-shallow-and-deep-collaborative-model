# Introduction
This is the matlab implementation of the paper "Visual Tracking via Shallow and Deep Collaborative Model"


***If you use this code in your research, please cite our paper:***

```
@article{zhuang2016visual,
  title={Visual tracking via shallow and deep collaborative model},
  author={Zhuang, Bohan and Wang, Lijun and Lu, Huchuan},
  journal={Neurocomputing},
  volume={218},
  pages={61--71},
  year={2016},
  publisher={Elsevier}
}

```


## Code
```
The code runs on Windows with MATLAB 2012b.

**The main tracking function is ./main/demo.m**

**The parameters related to image sequences are set in paraConfig_Our.m**

**Detailed explanations can be found in each file.**

```

## Copyright

Copyright (c) Bohan Zhuang. 2014

** This code is for non-commercial purposes only. For commerical purposes,
please contact Bohan Zhuang <zhuangbohan2013@gmail.com> **

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

