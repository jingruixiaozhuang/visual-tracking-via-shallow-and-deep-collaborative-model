%% modified by Bohan Zhuang
%% 05/10/2014
function results = run_Our(seq, res_path, bSaveImage)

addpath('./Affine Sample Functions');
addpath('./utils');
addpath('./estimation');
addpath('./toolbox');

para = paraConfig_Our(seq.name);
opt = para.opt;

%% 1.1 Initialize variables
rand('state',0);    randn('state',0);
%define the global parameters
global result;
 result = [];
global training_pos;
 training_pos = [];

%% 1.2 Load functions and parameters:
rect=seq.init_rect;
param0 = [rect(1)+rect(3)/2, rect(2)+rect(4)/2, rect(3)/32, 0, rect(4)/rect(3), 0]'; 
param0 = affparam2mat(param0);
opt.p0 = rect(4)/rect(3);

InitialNum = 10;
TotalFrameNum = seq.len;  %% Total frame number
%frame = imread(seq.s_frames{1});    
frame = seq.data.data(:,:,1);%%Load the first frame

if  size(frame,3) == 3
    framegray = double(rgb2gray(frame))/256;    %%For color images
else
    framegray = double(frame)/256;              %%For Gray images
end	

%% Load functions and parameters:
%%*************************************************************************
tmpl.mean = warpimg(framegray, param0, opt.tmplsize);       %%��ȡǰ��ͼ��� tmpl.mean<32*32>
nBlockNum=[4,4]; %%��tmpl.mean�ֳ�16��,ÿһ�����temp.blockmean��
BlockIndex=obtain_block_index(size(tmpl.mean),nBlockNum);%%����õ�ÿ�����귶Χ
tmpl.blockmean=cell(nBlockNum(1),nBlockNum(2));
wimgs = cell(nBlockNum(1),nBlockNum(2));
for i=1:nBlockNum(1)
    for j=1:nBlockNum(2)
       tmpl.blockmean{i,j}=tmpl.mean(BlockIndex{i,j}(1):BlockIndex{i,j}(2),BlockIndex{i,j}(3):BlockIndex{i,j}(4)); %�õ�ͼ���
    end 
end

param.wimg = tmpl.blockmean; 
tmpl.basis=cell(nBlockNum(1),nBlockNum(2));             %%��
tmpl.eigval = cell(nBlockNum(1),nBlockNum(2));          %%����ֵ
tmpl.numsample = 0;                                     %%����������
tmpl.reseig = 0;                                        %%��������
sz = size(tmpl.mean);  
N = sz(1)*sz(2);                                        %%��ά��

param = [];
param.est = param0;   %%����������                              %%tracking resultͼ���洢
param.occlusion_index=[];

% draw initial track window   
if bSaveImage
drawopt = drawtrackresult([], 1, frame, tmpl, param);
imwrite(frame2im(getframe(gcf)),sprintf('%s/%04d.jpg',res_path,1));
end

disp('resize the window as necessary, then press any key..'); %pause;
drawopt.showcondens = 0;  drawopt.thcondens = 1/opt.numsample;
%wimgs = cell(nBlockNum(1),nBlockNum(2));


%% load the nn parameters
load trainparam.mat
nn = nnsetup([1024 256 64 16 1]);
for i = 1 : 4
   nn.W{i} = W{i};
end


%% initial the tracker 
duration = 0; tic
f=1;
result = [result; param0']; 

%the initial training set
training_pos_initial = samplePos5(framegray, param0', opt.tmplsize); 
training_neg = SampleTrainingNeg(framegray, f, result, opt);
train_set = cat(1, training_pos_initial, training_neg);

%the label 
training_pos_label = ones(size(training_pos_initial,1),1); 
training_neg_label =  zeros(size(training_neg,1), 1);
train_label = cat(1, training_pos_label, training_neg_label);
% fine-tune the network
opts.batchsize = 50;
nn = nntrain(nn,train_set, train_label, opts);

training_pos = training_pos_initial;
duration = duration + toc;

%% track the sequence from frame 2 onward


 for f = 2:TotalFrameNum
    %%2.1 Load the (num)-th frame
%    frame = imread(seq.s_frames{f});
    frame = seq.data.data(:,:,f);
    if  size(frame,3) == 3
        framegray = double(rgb2gray(frame))/256;
    else
        framegray = double(frame)/256;
        frame = cat(3, [], frame, frame, frame);
    end
  % do tracking
   param.num=f;
   imgshow=cell(nBlockNum(1),nBlockNum(2));
   
  param.param = repmat(affparam2geom(param.est(:)), [1, opt.numsample]); 
 param.param = param.param + randn(6, opt.numsample).*repmat(opt.affsig(:),[1, opt.numsample]);
 warpimgs = warpimg(framegray, affparam2mat(param.param), size(tmpl.mean));  
   
   % do tracking
   tic;
  [param, maxidx, updatetemplate, update] = estwarp_condens(tmpl, param, opt, nBlockNum, imgshow, nn, warpimgs);  %%�������������
  result = [result; param.est'];
  
  training_pos_update= samplePos5(framegray,result(f,:), opt.tmplsize);  
 
  if f>InitialNum
  training_pos((mod(f - 1, 5)+5)*5+1:(mod(f - 1, 5)+5)*5+5, :) = training_pos_update;
  else 
   training_pos = [training_pos; training_pos_update]; 
  end
  
 
  %%  *****************update nn*********************
  if update ==1
  training_neg = SampleTrainingNeg(framegray, f, result, opt);

    % count = param.num;
     training_pos_label = ones(size(training_pos,1),1); 
     training_neg_label = zeros(size(training_neg,1), 1);
      
    train_set = cat(1, training_pos, training_neg);
    train_label = cat(1, training_pos_label, training_neg_label);

      
    %fine-tune the dbn framework
    opts.batchsize = 50;
    nn = nntrain(nn,train_set, train_label, opts);
    training_pos_update = [];
  end   
  
 if bSaveImage
 drawopt = drawtrackresult(drawopt, f, frame, tmpl, param);  
 imwrite(frame2im(getframe(gcf)),sprintf('%s/%04d.jpg',res_path,f));
 end
 
 
  %%*******************************��ݸ���********************************%%
   for i=1:nBlockNum(1)
     for j=1:nBlockNum(2)
         if param.update_index(i,j)==1
            wimgs{i,j} = [wimgs{i,j}, param.wimg{i,j}(:)];   %%��ݿ��ۻ�
            if (size(wimgs{i,j},2) >= opt.batchsize)       %%�ۻ�һ���̶ȡ�����
               [tmpl.basis{i,j}, tmpl.eigval{i,j}, tmpl.blockmean{i,j}, tmpl.numsample] = ...
               sklm(wimgs{i,j}, tmpl.basis{i,j}, tmpl.eigval{i,j}, tmpl.blockmean{i,j}, tmpl.numsample, opt.ff); %%sklm:   incremental SVD
               wimgs{i,j} = [];     %%��ݿ���� 
               if (size(tmpl.basis{i,j},2) > opt.maxbasis)          %%����������������
                   tmpl.reseig = opt.ff * tmpl.reseig + sum(tmpl.eigval{i,j}(opt.maxbasis+1:end));  %%��������--���������׹���
                   tmpl.basis{i,j}= tmpl.basis{i,j}(:,1:opt.maxbasis);   %%��ض�
                   tmpl.eigval{i,j}= tmpl.eigval{i,j}(1:opt.maxbasis);    %%����ֵ�ýضϣ�
               end
            end
          end
       end   
   end
   duration = duration + toc; 
end
%% store the result

fps = (TotalFrameNum-1)/duration;

% save the benchmark result

results.res=result;
results.type='ivtAff';
results.tmplsize = opt.tmplsize;
results.fps = fps;

%fileName2 = sprintf('results/%s_benchmark.mat', seq.name);
%save( fileName2, 'results');
%disp(['fps: ' num2str(fps)])
%%*************************3.STD Results*****************************%%
ourCenterAll  = cell(1,TotalFrameNum);      
ourCornersAll = cell(1,TotalFrameNum);
for num = 1:TotalFrameNum
    if  num <= size(result,1)
        est = result(num,:);
        [ center corners ] = p_to_box([32 32], est);
    end
    ourCenterAll{num}  = center;      
    ourCornersAll{num} = corners;
end

fileName1=sprintf('../results_discriminative/%s_our_rs.mat',seq.name);
save( fileName1, 'ourCenterAll', 'ourCornersAll','fps');




