
function tmpl = samplePos5(frame, param0, sz)
    tmpl = [];
    i = -1; j = 0;
            temp = warpimg(frame, param0 + [i  j 0 0 0 0], sz);
            temp = reshape(temp, 1, prod(sz));
            tmpl = [tmpl; temp];
 i = 1; j = 0;
           temp = warpimg(frame, param0 + [i  j 0 0 0 0], sz);
            temp = reshape(temp, 1, prod(sz));
            tmpl = [tmpl; temp];
            
     i = 0; j = -1;
             temp = warpimg(frame, param0 + [i  j 0 0 0 0], sz);
            temp = reshape(temp, 1, prod(sz));
            tmpl = [tmpl; temp];
     i = 0; j = 1;
         temp = warpimg(frame, param0 + [i  j 0 0 0 0], sz);
            temp = reshape(temp, 1, prod(sz));
            tmpl = [tmpl; temp];
    
            temp = warpimg(frame, param0, sz);
   temp = reshape(temp, 1, prod(sz));
    tmpl = [tmpl; temp];
end