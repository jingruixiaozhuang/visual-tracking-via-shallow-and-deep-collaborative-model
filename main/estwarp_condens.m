function [param, maxidx, updatetemplate, update] = estwarp_condens(tmpl, param, opt, BlockNum, imgshow, nn1, warpimgs)
%%
%global calculate_labels
%global calculate_pconfidence
%global calculate_nconfidence
%%****************实验改动***************%%


n = opt.numsample;  %粒子数
candidates = zeros(n,opt.tmplsize(1)*opt.tmplsize(2));
tsize=size(tmpl.mean);  % tmpl.mean为前景目标框
patchsz = size(tmpl.blockmean{1,1}); %tmpl.blockmean存储分割后的图像块patch
N = patchsz(1)*patchsz(2);%%N为每一小patch块的尺寸



%confidence_label = zeros(opt.numsample,1);
%%*****************************************************
%if f<=10
   % temp1 = reshape(wimgs, size(wimgs,1)*size(wimgs,2),size(wimgs,3));
    %tempt2 = temp1';
    %train_x = [train_x; tempt2];
    %output = train_x;
%end
%if f==10
% test_x = train_x;
%save test test_x;
%end
%%******************************************************
%save wimgs
BlockIndex=obtain_block_index(tsize, BlockNum);%对wings（32*32*200）进行分块

%%**************************************************
%将所有的粒子分块
blockwimgs=cell(BlockNum(1),BlockNum(2));
    for i=1:BlockNum(1)
        for j=1:BlockNum(2)
        block=warpimgs(BlockIndex{i,j}(1):BlockIndex{i,j}(2),BlockIndex{i,j}(3):BlockIndex{i,j}(4),:);  %%将tmpl.mean根据BlockIndex分成4块 先行后列，以tmpl.blockmean的第三维区分
        blockwimgs{i,j}=reshape(block,[patchsz(1),patchsz(2)*n]);
        end
    end
%%***********重构误差计算*****************************************
%% tmpl.basis is the ceter vector  tmpl.blockmean is the subspace
param.blocconf=cell(BlockNum(1),BlockNum(2));%%开始分块计算重构误差
coef=cell(BlockNum(1),BlockNum(2));
for i=1:BlockNum(1)
    for j=1:BlockNum(2)
       diff=repmat(tmpl.blockmean{i,j}(:),[1,n]) - reshape(blockwimgs{i,j},[N,n]);%%对各块分别计算，tmpl.mean可以将第三维设为4（分块数）.把一个图像块按列存储，一共n个candidates
       if (size(tmpl.basis{1,1},2) > 0) %cell的四个元素同时有值，所以只取一个便可
            diff_e=repmat(sum(diff.^2),N,1);
            coef{i,j} = tmpl.basis{i,j}'*diff;
            recon=tmpl.basis{i,j}*coef{i,j};  
            diff=diff-recon;  
        end
          param.blockconf{i,j}=exp(-sum(diff.^2)./opt.condenssig)';    
    end   
end
%%********************遮挡面具的使用*************************************
conf=zeros(n,1); 
nindex=size(param.occlusion_index,1);  


%if nindex<=4
 %  updatesign =0;
%else
  %  updatesign = 1;
%end

%%统计没被遮挡patch块的数目,param.occlusion_index为标号
if nindex==0                                %%全部为遮挡(注意全部为遮挡的时候误差是都算上的，和初始帧的计算相同）
   nindex=BlockNum(1)*BlockNum(2);
   k=1;
   for i=1:BlockNum(1)
       for j=1:BlockNum(2)
           param.occlusion_index(k,:)=[i,j];
           k=k+1;
       end
   end
end
for i=1:nindex
    a=param.occlusion_index(i,:);
    conf=conf+param.blockconf{a(1),a(2)}; %%对应论文中的(3)式
end

param.conf=conf./nindex;   %每个candidate对应的score值，和最后DBN分类器结合起来


param.occlusion_index_temp=param.occlusion_index;
tempsize = size(param.occlusion_index_temp,1);
param.occlusion_index=[];

for i=1:BlockNum(1)
    for j=1:BlockNum(2)
        % param.wimg{i,j}=blockwimgs{i,j}(:,((maxidx-1)*patchsz(2)+1):maxidx*patchsz(2)); %提取result candidate
           imgshow{i,j} = blockwimgs{i,j};
    end
end

   
% 画图****************************************

%canocclusion = zeros(patchsz(1)*BlockNum(1), patchsz(2)*BlockNum(2));
%canocclusion = zeros(patchsz(1)*BlockNum(1)*BlockNum(2), n*patchsz(2));

for i=1:BlockNum(1)
    for j=1:BlockNum(2)
     %canocclusion(((i-1)*patchsz(1)+1:i*patchsz(1)), ((j-1)*patchsz(2)+1: j*patchsz(2)))=imgshow{i,j};
      canocclusion((i-1)*BlockNum(2)*patchsz(1)+(j-1)*patchsz(2)+1:(i-1)*BlockNum(2)*patchsz(1)+j*patchsz(2),:)=imgshow{i,j}; 
    end
end


for k = 1:n
    temp = zeros(BlockNum(2)*BlockNum(1)*patchsz(1),patchsz(2));
    img_temp = zeros(patchsz(1)*BlockNum(1),patchsz(2)*BlockNum(2));
    temp= canocclusion(:,(k-1)*patchsz(2)+1: k*patchsz(2));
    for i=1:BlockNum(1)
        for j=1:BlockNum(2)
             img_temp((i-1)*patchsz(1)+1:i*patchsz(1),(j-1)*patchsz(2)+1:j*patchsz(2)) = temp((i-1)*BlockNum(1)*patchsz(1)+(j-1)*patchsz(2)+1:(i-1)*BlockNum(2)*patchsz(1)+j*patchsz(2),:);
        %imwrite(img_temp,strcat('E:\毕业设计\参考demo\tracking\Occlusion detection\tu\',num2str(k),'.jpg'));
        end
    end
    candidates(k,:) = reshape(img_temp, 1, opt.tmplsize(1)*opt.tmplsize(2));
    
end

%% **********************The estimation layer**************************************
 nnconfidence = nnpredict(nn1, candidates);
if max(nnconfidence)<0.8
   update =1;
else
    update =0;
end

%if max(nn2confidence)<0.8
 %   update(2) =1;
%else
  %  update(2)=0;
%end
%%*******************The integral confidence score*****************************
%param.conf = param.conf.*nnconfidence;
param.conf = nnconfidence;
%param.conf = nnconfidence;

%%********************** 选取最佳粒子 ************************
%param.conf=sqrt(1-param.conf); %%CIE改动
[~,maxidx] = max(param.conf);  %%CIE改动

% the occlusion ratio--> update nn
%param.occlusionratio = find(candidates(maxidx,:)==0)/prod(opt.tmplsize);

%% ********************形成新的遮挡面具以及更新条件**********************
param.everyblockconf=zeros(BlockNum(1),BlockNum(2));
param.update_index=zeros(BlockNum(1),BlockNum(2));


for i=1:BlockNum(1)
    for j=1:BlockNum(2)
        param.everyblockconf(i,j)=param.blockconf{i,j}(maxidx);  %得到best candidate每块的confident score   
         param.wimg{i,j}=blockwimgs{i,j}(:,((maxidx-1)*patchsz(2)+1):maxidx*patchsz(2)); %提取result candidate
        if (param.num<20)
              param.occlusion_index=cat(1,param.occlusion_index,[i,j]);
              param.update_index(i,j)=1;
            elseif (param.num>=20&&param.everyblockconf(i,j)>=3*10e-2)  %%设定“判为遮挡”的阈值
              param.occlusion_index=cat(1,param.occlusion_index,[i,j]);
              param.update_index(i,j)=1;    %%patch块更新标志
        else
              param.update_index(i,j)=0;%%update_index=1则模板更新，=0则不更新
        end 
   end
end


%imwrite(canocclusion,strcat('E:\毕业设计\参考demo\tracking\Occlusion detection\tu\',num2str(f),'.jpg'));

%**************************************************
% save the testdata
%warpimgs_temp = [];
%for i = 1:size(candidates, 1)
%temp = reshape(candidates(i,:), 1, opt.tmplsize(1)*opt.tmplsize(2)); 
%warpimgs_temp = [warpimgs_temp;temp];
%end
%save testdata  warpimgs_temp

param.est=affparam2mat(param.param(:,maxidx)); %the tracking result
updatetemplate = candidates(maxidx, :);




%%******************实验部分***********%%
%  exwimgs=wimgs(:,:,maxidx);
% %white=ones(32,32);
% % 
% %figure('position',[0 0 320 240])
% clf; 
% %set(gcf,'DoubleBuffer','on','MenuBar','none');
% colormap('gray');
% axes('position', [0.00 0 1.00 1.0]);
% %imagesc( exwimgs, [0,1]);
% hold on;
% % 
% [interestHIST,fhist]=sifthist(exwimgs,BlockIndex,1,param.bags); %% 统计各粒子的直方图信息
% pause(0.5);
% if f==7||f==14||f==70||f==74||f==77
%  hist1=cat(2,hist1,fhist);
% end
% imwrite(frame2im(getframe(gcf)),sprintf('siftpoint/%03d.png',f));
%绘制直方图
%%******************实验部分***********%%  


