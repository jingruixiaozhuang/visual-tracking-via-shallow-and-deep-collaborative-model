% script: trackparam.m
%     loads data and initializes variables
%

% Copyright (C) Jongwoo Lim and David Ross.
% All rights reserved.

% DESCRIPTION OF OPTIONS:
%
% Following is a description of the options you can adjust for
% tracking, each proceeded by its default value.

%*************************************************************
% For a new sequence , you will certainly have to change p.
%       对于一个新的视频序列，需要改变的是p的值
%*************************************************************
%
% To set the other options,
% first try using the values given for one of the demonstration
% sequences, and change parameters as necessary.
%
%*************************************************************
% p = [px, py, sx, sy, theta]; 
% The location of the target in the first frame.
%       目标在第一帧的位置
% px and py are th coordinates of the centre of the box
%   px，py       ：   目标框的中心位置；
%
% sx and sy are the size of the box in the x (width) and y (height)
%   dimensions, before rotation
%   sx，sy       ：   目标框的宽，高
%
% theta is the rotation angle of the box
%   theta        ：   目标框的旋转角度
%
% 'numsample',600,   The number of samples used in the condensation
% algorithm/particle filter.  Increasing this will likely improve the
% results, but make the tracker slower.
%   'numsample'  :    采样次数，采样次数增加会提高效果，但跟踪速度会很慢
%
% 'condenssig',0.25,  The standard deviation of the observation likelihood.
%   'condenssig  ：   观测对象似然标准偏差
%
% 'ff',1, The forgetting factor, as described in the paper.  When
% doing the incremental update, 1 means remember all past data, and 0
% means remeber none of it.
%   'ff'         ：   遗忘因子
%
% 'batchsize',5, How often to update the eigenbasis.  We've used this
% value (update every 5th frame) fairly consistently, so it most
% likely won't need to be changed.  A smaller batchsize means more
% frequent updates, making it quicker to model changes in appearance,
% but also a little more prone to drift, and require more computation.
%   'batchsize'  ：   更新间隔
%
% 'affsig',[4,4,.02,.02,.005,.001]  These are the standard deviations of
% the dynamics distribution, that is how much we expect the target
% object might move from one frame to the next.  The meaning of each
% number is as follows:
%   'affsig'    ：   动态模型，affine变换参数分布"均值","方差"
%    affsig(1) = x translation (pixels, mean is 0)
%    affsig(2) = y translation (pixels, mean is 0)
%    affsig(3) = rotation angle (radians, mean is 0)
%    affsig(4) = x scaling (pixels, mean is 1)
%    affsig(5) = y scaling (pixels, mean is 1)
%    affsig(6) = scaling angle (radians, mean is 0)
%
% OTHER OPTIONS THAT COULD BE SET HERE:
%
% 'tmplsize', [32,32] The resolution at which the tracking window is
% sampled, in this case 32 pixels by 32 pixels.  If your initial
% window (given by p) is very large you may need to increase this.
%   'tmplsize'  ：   跟踪窗大小
%
% 'maxbasis', 16 The number of basis vectors to keep in the learned
% apperance model.
%   'maxbasis'  :   最大基向量个数
%
% Change 'title' to choose the sequence you wish to run.  If you set
% title to 'dudek', for example, then it expects to find a file called 
% dudek.mat in the current directory.
%
% Setting dump_frames to true will cause all of the tracking results
% to be written out as .png images in the subdirectory ./dump/.  Make
% sure this directory has already been created.

%*************************************************************

  title='face_sequence'; 
%  title='woman'; 
%  title='faceocc2';  
%   title='car4';   
%   title = 'Dudek'; 
%  title='car11';  
%  title='singer1'; 
%   title='girl';
%  title='animal';  
%  title = 'basketball'; 
%  title = 'jumping'; 
%  title = 'sylv';  
%  title = 'liquor'; 
%   title = 'football';  
%  title = 'bolt';   
%  title = 'lemming';  
%  title = 'DavidOutdoor';  
%  title = 'shaking';   
%  title = 'soccer';  
%  title = 'matrix';
%  title = 'skating1'; 
%   title = 'mountainbike';  
%   title = 'Trellis';  
%   title = 'bird';   
%   title = 'boy';  
%  title = 'walking2';  
%  title = 'walking'; 
%  title = 'couple';  
%  title = 'Mhyang';  
%  title = 'suv';     
%  title = 'tiger1'; 
%  title = 'tiger2'; 
%  title = 'dog';    
%   title = 'Fish';   
%   title = 'coke';     
%  title = 'freeman1';   
 %  title = 'freeman3';  
%  title = 'freeman4';  
%  title = 'skiing';    
%   title = 'David2';   
%  title = 'doll2';     
%  title = 'jogging';  
%   title = 'MotorRolling'; 
%  title = 'football1';  
%  title = 'crossing';   
%  title = 'CarScale';   
%  title = 'fleetface';  
%  title = 'David';   
%  title = 'ironman';  
%   title = 'subway';  
 %  title = 'singer2';

%************************************************************
global p0;
%global lamda;
global num_n;
%*************************************************************
 dump_frames = true;    %%true  ：   保存每次跟踪的结果
% dump_frames = false;    %%true  ：   保存每次跟踪的结果
%*************************************************************


switch (title)         
    
   case 'singer2';  p = [332,210,67,122, 0.0];
    opt = struct('numsample',600, 'condenssig',0.07, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.008,.000,.000,.000]);         
    
    
    
  case 'jogging';  p = [199,136,37,114, 0.0];
   
     opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
    
  
    case 'subway';  p = [26,114,19,51, 0.0];
    opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
             
    
    
   case 'ironman';  p = [231,114,49,57, 0.0];
    opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
            
              
              
   case 'doll2';  p = [162,187,32,73, 0.0];
     opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                      
    
    
    
    case 'David';  p = [195,115,64,78, 0.0];
    opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
             
              
    
    
    case 'fleetface';  p = [466,330,122,148, 0.0];
     opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                     
  
    
    
    case 'CarScale';  p = [27,179,42,26, 0.0];
     opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                     
    
    
    
    case 'football1';  p = [166,127,26,43, 0.0];
   opt = struct('numsample',600, 'condenssig',0.07, 'ff',1, ...
                  'batchsize',5, 'affsig',[6,6,.01,.000,.000,.000]);         
             
              
              
     case 'crossing';  p = [214,176,17,50, 0.0];
    opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                        
              
              
    
              
   case 'MotorRolling';  p = [178,131,122,125, 0.0];
   opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
             
    
    
    case 'David2';  p = [155,90,27,34, 0.0];
    opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                             
            
            
      
    case 'freeman1';  p = [265,80,23,28, 0.0];
  opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                      
     
         
   case 'freeman3';  p = [251,71,12,13, 0.0];
   opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                           
              
         
    case 'freeman4';  p = [133,92,15,16, 0.0];
   opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                             
              
      
    case 'skiing';  p = [461,194,29,26, 0.0];
    opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                               
            
            
            
   case 'coke';     p = [322,200,48,80,0.0];
      opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                     
            
                
     
   case 'dog';  p = [165,130,51,36, 0.0];
     opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                 
            
            
   case 'Fish';  p = [164,99,60,88, 0.0];
    opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                          
            
            
       case 'tiger2';  p = [66,99,68,78, 0.0];
     opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                   
            
            
       case 'tiger1';  p = [270,130,76,84, 0.0];
   opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                            
            
  
      case 'suv';  p = [188,145,91,40, 0.0];
     opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
             
            
            
    case 'Mhyang';  p = [115,88,62,70, 0.0];
    opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                      
              
            
  case 'couple';  p = [64,78,25,62, 0.0];
    opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
             
              
              
  case 'walking';  p = [704,479,24,79, 0.0];
    opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                          
            
        
            
   case 'walking2';  p = [146,190,31,115, 0.0];
   opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                             
            
            
            
  case 'boy';  p = [306,164,35,42, 0.0];
  opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[8,8,.01,.000,.000,.000]);                     
            

  case 'Dudek';  p = [188,192,110,130, 0.0];
    opt = struct('numsample',600, 'condenssig',0.07, 'ff',1, ...
                  'batchsize',5, 'affsig',[6,6,.01,.000,.000,.000]);                    
            
                       
            
case 'bird';  p = [116 254 68 72 0.0]; 
    opt = struct('numsample',600, 'condenssig',0.07, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.008,.000,.001,.000]);     


case 'doll';  p = [162,187,32,73,0.0];
   opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
             


  case 'mountainbike';  p = [353,213,67,56,0.0];
     opt = struct('numsample',600, 'condenssig',0.07, 'ff',1, ...
                  'batchsize',5, 'affsig',[6,6,.01,.000,.000,.000]);         
             



case 'skating1';     p = [179,230,34,84,0.0];
      opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                     


 case 'matrix'; p = [350,60,38,42,0];
    opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                     
    
  

    case 'soccer';  p = [336,176,67,81,0];
 opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
              
              
    case 'Trellis';  p = [180,105,68,101,0];
       opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
    
              
    
 case 'shaking';p = [255, 170, 60, 70, 0 ];
   opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                     
              
 case 'DavidOutdoor';  p = [102,266,40,134,0.00];
          opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                     
    

    case 'liquor'; p = [ 292, 255, 68, 202, 0.00];
             opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
    

  case 'lemming';    p = [72,252,60,112, 0.00];
     opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                      
    
    
    
   case 'basketball';      p = [210,260,40,80,0.0];
     opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                    
              
  case 'football';   p = [330, 130, 50, 55, 0];
        opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.001,.000,.000,.000]);         
                     
              

   
   case 'sylv';         p = [145 81 53 53 -0.2];  
     opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                            
    

             
case 'fish';  p = [165 102 62 80 0];
     opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                     
              
             
case 'car4';  p = [245 180 200 150 0];
    opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                   
              
             
case 'car11';  p = [89 140 30 25 0];
       opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                   
             

           
 case 'face_sequence';p=[ 177,147,115,145,0];
     opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                    
              

   case 'bolt';         p = [352,192,28,57,0.0];
   opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                    
              
   
  case 'woman'; p = [224 169 21 95 0.0];
       opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);  
              
             
  case 'faceocc2';     p = [159,106,82,98,0.00];
      opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                   
  
 
  case 'singer1';  p = [100, 200, 100, 300, 0];
       opt = struct('numsample',600, 'condenssig',0.07, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.008,.000,.000,.000]);         
             
              
    case 'girl';  p = [73,44,31,45,0];
      opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                    
           
              
   case 'animal';   p = [350, 40, 100, 70, 0];
       opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                     
              
         
   case 'jumping';          p = [163,126,33,32,0];
      opt = struct('numsample',600, 'condenssig',0.10, 'ff',1, ...
                  'batchsize',5, 'affsig',[7,7,.01,.000,.000,.000]);         
                              
  
       
             
otherwise;  error(['unknown title ' title]);
end

%%***************************Load Data*****************************%%
if (~exist('datatitle') | ~strcmp(title,datatitle))     
  if (exist('datatitle') & ~strcmp(title,datatitle))    %%数据不匹配
    disp(['title does not match.. ' title ' : ' datatitle ', continue?']);
    pause;
  end
  disp(['loading ' title '...']);
  clear truepts;
  load(['Data\\' title '.mat'],'data');   %%load data  ,'datatitle','truepts'
  %************示RGB图改动部分*************%
 load(['ColorData\\' title '.mat'],'colordata');
    %************示RGB图改动部分*************%                                      %%data      ：   待跟踪数据
                                        %%truepts   ：   关键点坐标
end
%%***************************Load Data*****************************%%

%%**************************初始仿射参数*****************************%%
param0 = [p(1), p(2), p(3)/32, p(5), p(4)/p(3), 0]';     %%p = [px, py, sx, sy, theta];   
param0 = affparam2mat(param0);
p0 = p(4)/p(3);
InitialNum = 10;
%lamda = 0.5;
num_n = 100;
%%**************************初始仿射参数*****************************%%

% opt.dump = dump_frames;
% if (opt.dump & exist('dump') ~= 7)
%   error('dump directory does not exist.. turning dump option off..');
%   opt.dump = 0;
% end
% Setting dump_frames to true will cause all of the tracking results
% to be written out as .png images in the subdirectory ./dump/.  Make
% sure this directory has already been created.

