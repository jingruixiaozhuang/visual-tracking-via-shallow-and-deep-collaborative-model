%% **************************************sample the negative training samples around the target****************************************
%param.param�����param.temp
function training_neg = SampleTrainingNeg(frame, f, result, opt)


n = opt.num_n; % Sampling Number
param.est = result(end,:)';
param.param0 = zeros(6,n);      % Affine Parameter Sampling
param.temp = zeros(6,n);
param.param0 = repmat(affparam2geom(param.est(:)), [1,n]);
randMatrix = randn(6,n);
sigma = [round(opt.tmplsize(2)*param.est(3)), round(opt.tmplsize(1)*param.est(3)*opt.p0), .000, .000, .000, .000];
param.temp = param.param0 + randMatrix.*repmat(sigma(:),[1,n]);
 
back = round(sigma(1)/4);
center = param.param0(1,1);
left = center - back;
right = center + back;
nono = param.temp(1,:)<=right&param.temp(1,:)>=center;
param.temp(1,nono) = right;
nono = param.temp(1,:)>=left&param.temp(1,:)<center;
param.temp(1,nono) = left;
 
back = round(sigma(2)/4);  % original  back = round(sigma(2)/3);
center = param.param0(2,1);
top = center - back;
bottom = center + back;
nono = param.temp(2,:)<=bottom&param.temp(2,:)>=center;
param.param(2,nono) = bottom;
nono = param.temp(2,:)>=top&param.temp(2,:)<center;
param.temp(2,nono) = top;
 
o = affparam2mat(param.temp);     % Extract or Warp Samples which are related to above affine parameters
warpimgs = warpimg(frame, o, opt.tmplsize);
 
m = prod(opt.tmplsize);

for i = 1:n
    training_neg(i,:) = reshape(warpimgs(:,:,i), 1, m);   
end