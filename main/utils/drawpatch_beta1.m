%% 画原理示意图
%% 画图显示patch权重
clear;
% title = 'woman_sequence';
title = 'guo';
if ~isdir(['result\' [title, 'Beta1']])
    mkdir('result\',[title, 'Beta1']);
end
load1=sprintf('result\\%s\\%s_param.mat',title,title);
load2=sprintf('result\\%s\\%s.mat',title,title);
load(load1);
load(load2);
dataPath = [title '\'];


% nsize = [64 32];%woman_sequence%这里根据目标的形状要做更改
% vn=7;hn=3;
nsize = [32 32];
vn =3;hn=3;%这里根据目标的形状要做更改
nh=nsize(1);nw=nsize(2);


begin=1;
initbatch=5;
FrameNum=size(Ratio,1)+begin+initbatch;

name=sprintf('result\\%s\\%04d.jpg',title,begin+initbatch+1);
frame = imread(name);     %读入第一个跟踪结果-得到图像尺寸
figure('position',[30 50 size(frame,2) size(frame,1)]);
for f=1:FrameNum-(begin+initbatch)
    
    name=sprintf('result\\%s\\%04d.jpg',title,begin+initbatch+f);
    frame = imread(name);     %读入跟踪结果-原彩色图像
    %-------------------------
    %得到目标在原图中的框
    p=result(f+initbatch,:)';
    M = [p(1) p(3) p(4); p(2) p(5) p(6)];     %%affine变换参数
    corners = [ 1,-nw/2,-nh/2; 1,nw/2,-nh/2; 1,nw/2,nh/2; 1,-nw/2,nh/2; 1,-nw/2,-nh/2 ]';
    corners = M * corners;      %%顶点坐标,第一排是横坐标，第二排是纵坐标
    corners = round(corners);

    %--------------------------
    patchH=round(nh/(vn+1));%注意：这是patch的一半高度
    patchW=round(nw/(hn+1));%注意：这是patch的一半宽度
    WeightMat=zeros(nh,nw);
    for j=1:hn
       for i=1:vn
           if (i==1)&(j==1)%左上角的patch
               WeightMat(((i-1)*patchH+1) : ((i-1)*patchH+patchH) , ((j-1)*patchW+1):((j-1)*patchW+patchW)) = Beta1(f,((j-1)*vn+i));
           elseif j==1%左边边缘的patch
               WeightMat(((i-1)*patchH+1) : ((i-1)*patchH+patchH) , ((j-1)*patchW+1):((j-1)*patchW+patchW)) = 0.5*(Beta1(f,((j-1)*vn+i))+Beta1(f,((j-1)*vn+(i-1))));
           elseif i==1%上边边缘的patch
               WeightMat(((i-1)*patchH+1) : ((i-1)*patchH+patchH) , ((j-1)*patchW+1):((j-1)*patchW+patchW)) = 0.5*(Beta1(f,((j-1)*vn+i))+Beta1(f,((j-2)*vn+i)));
           else%中间部分
               WeightMat(((i-1)*patchH+1) : ((i-1)*patchH+patchH) , ((j-1)*patchW+1):((j-1)*patchW+patchW)) = 0.25*(Beta1(f,((j-1)*vn+i))+Beta1(f,((j-1)*vn+(i-1)))+Beta1(f,((j-2)*vn+i))+Beta1(f,((j-2)*vn+(i-1))));
           end
       end
    end
    %左下角
    WeightMat((vn*patchH+1) : end , 1:patchW) = Beta1(f,1*vn);
    %下边边缘的patch
    for j=2:hn
       WeightMat((vn*patchH+1) : end , ((j-1)*patchW+1):((j-1)*patchW+patchW)) = 0.5*(Beta1(f,((j-1)*vn+i))+Beta1(f,((j-2)*vn+i)));
    end
    %右上角的patch
    WeightMat(1:patchH , (vn*patchW+1):end) = Beta1(f,((hn-1)*vn+1));
    %右边边缘的patch
    for i=2:vn
       WeightMat(((i-1)*patchH+1) : ((i-1)*patchH+patchH) , (vn*patchW+1):end) = 0.5*(Beta1(f,((j-1)*vn+i))+Beta1(f,((j-1)*vn+(i-1))));
    end
    %右下角的patch
    WeightMat((vn*patchH+1) : end , (vn*patchW+1):end) = Beta1(f,vn*hn);   
    WeightMat=WeightMat/max(WeightMat(:));%简单归一化，使得看得更清楚
   
    ColorMat=WeightMat*255;
    ColorMat=uint8(ColorMat);
    demo = frame;
%--------------------------------------
%仿射变换用于旋转的框画patch
basept=corners(:,1:4)';%一行一对横纵坐标
inputpt=[1,1;nsize(2),1;nsize(2),nsize(1);1,nsize(1)];
t_concord = cp2tform(inputpt,basept,'projective');%转换矩阵
r=[1 nsize(1) 1 nsize(1)];%个点纵坐标
c=[1 nsize(2) nsize(2) 1];
[tempx,tempy]=tformfwd(t_concord,c,r);%得到转换后图像的边界坐标
registered=imtransform(ColorMat,t_concord);
xl=max(1,round(min(tempx)));xr=xl+size(registered,2)-1;
yt=max(1,round(min(tempy)));yb=yt+size(registered,1)-1;
demo(yt:yb,xl:xr,2)=registered;% 1--red, 2--green, 3--blue , frame(yt:yb,xl:xr,2)+registered;
%-------------------------------------------
    axes(axes('position', [0.00 0 1.00 1.0]));
    set(gcf,'DoubleBuffer','on','MenuBar','none'); 
    axis equal tight off; 
    imagesc(demo);
    imwrite(frame2im(getframe(gcf)),sprintf('result/%s/%04d.png',[title 'Beta1'],begin+initbatch+f));
end
