
function para=paraConfig_Our(title)

condenssig = 0.12;

opt = struct('numsample', 600, 'condenssig',condenssig, 'ff',1,...
    'batchsize',5,'affsig', [6,6,0.01,0.0,0.0,0.0]);

opt.maxbasis = 16;

opt.tmplsize = [32, 32];

opt.batchsize = 5;  %%积累图像块个数
opt.ff = 1.0;       %%遗忘因子
opt.errfunc = 'L2'; %%误差函数类型
opt.minopt = optimset; opt.minopt.MaxIter = 25; opt.minopt.Display='off';

opt.InitialNum = 10;
opt.num_n = 100;
opt.dump_frames = true; 

para.opt = opt;