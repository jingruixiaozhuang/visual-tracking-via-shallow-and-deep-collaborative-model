%%获取Block Index：
%%返回每个块的四个角点的坐标
function blockIndex = obtain_block_index(imSize, nBlockNum)
blockIndex = cell(nBlockNum(1), nBlockNum(2));
interval = [ floor(imSize(1)/nBlockNum(1)),....
             floor(imSize(2)/nBlockNum(2)) ]; %%确定分块大小
%%行方向
rPosition = zeros(1,nBlockNum(1)+1); %%若分为4*4，则将行坐标用5个值分为4段
rPosition(1) = 1; 
rPosition(nBlockNum(1)+1) = imSize(1)+1; 
for num = 2:nBlockNum(1)
    rPosition(num) = rPosition(num-1) + interval(1);
end      
%%列方向
cPosition = zeros(1,nBlockNum(2)+1);
cPosition(1) = 1; 
cPosition(nBlockNum(2)+1) = imSize(2)+1; 
for num = 2:nBlockNum(2)
    cPosition(num) = cPosition(num-1) + interval(2);
end
%%
for ii = 1:nBlockNum(1)
    for jj = 1:nBlockNum(2)
        rMin = rPosition(ii);
        rMax = rPosition(ii+1)-1;
        cMin = cPosition(jj);
        cMax = cPosition(jj+1)-1;
        blockIndex{ii,jj} = [ rMin, rMax, cMin, cMax ];
    end
end