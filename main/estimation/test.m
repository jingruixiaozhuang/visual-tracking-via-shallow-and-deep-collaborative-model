% load mnist_uint8;
% load online_data;

% normalize the data
train_x = double(train_x) / 255; 
test_x  = double(test_x)  / 255;
train_y = double(train_y);
test_y  = double(test_y);


rand('state',0);

%train dbn
dbn.sizes = [100 100];
opts.numepochs =   1;
opts.batchsize = 100;
opts.momentum  =   0;
opts.alpha     =   1;
dbn = dbnsetup(dbn, train_x, opts);
dbn = dbntrain(dbn, train_x, opts);

%unfold dbn to nn
nn = dbnunfoldtonn(dbn, 2);
nn.activation_function = 'sigm';

% train nn and fine-tune the parameters, then estimate the label for all the inputs
nn = nntrain(nn, train_x, train_y, opts);
[er, bad] = nntest(nn, test_x, test_y);

fprintf('er=%f\n', er);
assert(er < 0.10, 'Too big error');
