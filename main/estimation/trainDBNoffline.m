
load traindata.mat
train_set = frametotal;
load trainlabel.mat
train_label = label;

dbn.sizes = [100 50 20];
opts.batchsize = 50;
opts.momentum  =   0;
opts.alpha     =   1;
dbn = dbnsetup(dbn, train_set, opts);
dbn = dbntrain(dbn, train_set, opts);

%unfold dbn to nn
nn = dbnunfoldtonn(dbn, 1);  
nn.activation_function = 'sigm';

%get the test set
nn = nntrain(nn, train_set, train_label, opts);
W = nn.W;
save trainparam.mat W