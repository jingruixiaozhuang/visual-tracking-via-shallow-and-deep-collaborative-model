function [nn, L]  = nntrain(nn, train_x, train_y, opts, val_x, val_y)


assert(isfloat(train_x), 'train_x must be a float');

loss.train.e               = [];
loss.train.e_frac          = [];
loss.val.e                 = [];
loss.val.e_frac            = [];
opts.validation = 0;


fhandle = [];
if isfield(opts,'plot') && opts.plot == 1
    fhandle = figure();
end

%if f<=10
  %  numepochs = 50;
%else
numepochs = 20;
%end
    
m = size(train_x, 1);
%m是训练样本的数量
%注意在调用的时候我们设置了opt，batchsize是做batch gradient时候的大小
batchsize = opts.batchsize; 
numbatches = floor(m / batchsize);  %计算batch的数量


assert(rem(numbatches, 1) == 0, 'numbatches must be a integer');
L = zeros(numepochs*numbatches,1);
n = 1;
%numepochs是循环的次数
for i = 1 : numepochs
    tic;
    kk = randperm(m);
    %把batches打乱顺序进行训练，randperm(m)生成一个乱序的1到m的数组
    for l = 1 : numbatches
        batch_x = train_x(kk((l - 1) * batchsize + 1 : l * batchsize), :);
        %Add noise to input (for use in denoising autoencoder)
        %加入noise，这是denoising autoencoder需要使用到的部分
        %这部分请参见《Extracting and Composing Robust Features with Denoising Autoencoders》这篇论文
        %具体加入的方法就是把训练样例中的一些数据调整变为0，inputZeroMaskedFraction表示了调整的比例
        %if(nn.inputZeroMaskedFraction ~= 0)
            %batch_x = batch_x.*(rand(size(batch_x))>nn.inputZeroMaskedFraction);
        %end
        batch_y = train_y(kk((l - 1) * batchsize + 1 : l * batchsize), :);
        %这三个函数
        %nnff是进行前向传播，nnbp是后向传播，nnapplygrads是进行梯度下降
        %我们在下面分析这些函数的代码
        nn = nnff(nn, batch_x, batch_y);
        nn = nnbp(nn);
        nn = nnapplygrads(nn);
        L(n) = nn.L;
        n = n + 1;
    end
    
    t = toc;
    if ishandle(fhandle)
        if opts.validation == 1
            loss = nneval(nn, loss, train_x, train_y, val_x, val_y);
        else
            loss = nneval(nn, loss, train_x, train_y);
        end
        nnupdatefigures(nn, fhandle, loss, opts, i);
    end
        
    disp(['epoch ' num2str(i) '/' num2str(numepochs) '. Took ' num2str(t) ' seconds' '. Mean squared error on training set is ' num2str(mean(L((n-numbatches):(n-1))))]);
    nn.learningRate = nn.learningRate * nn.scaling_learningRate;
    %if max(nn.a{end})>0.9
       % break;
   % end
end