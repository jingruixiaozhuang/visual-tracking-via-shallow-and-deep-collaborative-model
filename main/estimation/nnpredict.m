function nnconfidence = nnpredict(nn, x)
    nn.testing = 1;
    nn = nnff(nn, x, zeros(size(x,1), nn.size(end)));
    nn.testing = 0;
    %[~, labels] = max(nn.a{end},[],2);
    %pos_idx = find(round(nn.a{end})==1);
    %neg_idx = find(round(nn.a{end})==0);   
    nnconfidence = nn.a{end};
end
