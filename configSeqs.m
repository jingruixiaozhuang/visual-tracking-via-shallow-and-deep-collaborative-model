function seqs=configSeqs

seqVTD={struct('name','bird','path','E:\paper\benchmark\benchmark data\bird\','startFrame',1,'endFrame',99,'nz',4,'ext','jpg','init_rect', [0,0,0,0]),...
    struct('name','board','path','E:\paper\benchmark\benchmark data\board\','startFrame',1,'endFrame',698,'nz',4,'ext','jpg','init_rect', [0,0,0,0]) };

seqIVT={ struct('name','sylvester2008','path','E:\paper\benchmark\benchmark data\sylvester\','startFrame',1,'endFrame',367,'nz',4,'ext','jpg','init_rect', [0,0,0,0])};

seqOther={struct('name','box','path','E:\paper\benchmark\benchmark data\box\','startFrame',1,'endFrame',1161,'nz',4,'ext','jpg','init_rect', [0,0,0,0])};

seqs=[seqIVT,seqVTD,seqOther];

